//
//  FAPlayer.m
//  TileMapDemo
//
//  Created by Mark Brownsword on 7/03/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FAPlayer.h"
#import "FAPathStep.h"

@interface FAPlayer ()

@property (nonatomic, assign, readwrite) PlayerState state;
@property (nonatomic, assign) CGPoint startPosition;
@property (nonatomic, assign) CGPoint lastPosition;
@property (nonatomic, strong) UIImage *atlas;
@property (nonatomic, strong) SKAction *forwardMoveStrategy;
@property (nonatomic, strong) SKAction *leftMoveStrategy;
@property (nonatomic, strong) SKAction *backwardMoveStrategy;
@property (nonatomic, strong) SKAction *rightMoveStrategy;
@property (nonatomic, assign) MoveDirection currentMoveDirection;

@end

@implementation FAPlayer {
@private
    
}

static NSString * const kPlayerMoveSequenceKey = @"playerMoveSequence";
static NSString * const kPlayerAnimationKey = @"playerAnimation";

- (instancetype)initWithAtlas:(UIImage *)atlas atPosition:(CGPoint)position {
    return [self initWithAtlas:atlas atPosition:position withStartPosition:position];
}

- (instancetype)initWithAtlas:(UIImage *)atlas atPosition:(CGPoint)position withStartPosition:(CGPoint)startPosition {
    if ((self = [super init])) {
        self->_state = PlayerStateIdle;
        self->_atlas = atlas;
        self->_lastPosition = position;
        self->_startPosition = startPosition;
        
        self->_forwardMoveStrategy = [self forwardMoveStrategy];
        self->_leftMoveStrategy = [self leftMoveStrategy];
        self->_backwardMoveStrategy = [self backwardMoveStrategy];
        self->_rightMoveStrategy = [self rightMoveStrategy];
        
        self.texture = [self idleTextureForPlayer];
        self.size = self.texture.size;
        self.position = self->_lastPosition;
    }
    
    return self;
}


#pragma mark ***** Public Properties *****

- (BOOL)isIdle {
    return self.state == PlayerStateIdle;
}

- (BOOL)hasPendingMove {
    return self.state == PlayerStatePendingMove;
}


#pragma mark ***** Public Methods *****

- (void)moveAlongPath:(NSMutableArray *)pathSteps {
    if (pathSteps == nil || pathSteps.count == 0) {
        return;
    }
    
    NSMutableArray *actions = [NSMutableArray array];
    for (FAPathStep *step in pathSteps) {
        SKAction *moveAction = [SKAction moveTo:step.position duration:1.0];
        [actions addObject:moveAction];
    }
    
    [self changeStateTo:PlayerStateMoving];
    
    [actions addObject:[SKAction performSelector:@selector(changeStateToPlayerStateStopping) onTarget:self]];
    [self runAction:[SKAction sequence:[actions copy]] withKey:kPlayerMoveSequenceKey];
}

- (void)forceStop {
    if (self.state != PlayerStateForceStop) {
        return;
    }
    
    SKSpriteNode *tile = [self.delegate currentTile];
    if (!CGPointEqualToPoint(self.position, tile.position)) {
        return; // Return if not at tile centre
    }
    
    [self removeActionForKey:kPlayerMoveSequenceKey];
    [self changeStateTo:PlayerStateStopping];
}

- (void)changeStateTo:(PlayerState)state {
    if (![self canChangeStateTo:state]) {
        return;
    }
    
    self.state = state;
}

- (BOOL)canChangeStateTo:(PlayerState)state {
    switch (state) {
        case PlayerStateIdle:
            return self.state == PlayerStateStopping;
        case PlayerStatePendingMove:
            return self.state == PlayerStateIdle ||
                   self.state == PlayerStateMoving;
        case PlayerStateMoving:
            return self.state == PlayerStatePendingMove;
        case PlayerStateForceStop:
            return self.state == PlayerStateMoving;
        case PlayerStateStopping:
            return self.state == PlayerStatePendingMove ||
                   self.state == PlayerStateMoving ||
                   self.state == PlayerStateForceStop;
    }
}

- (void)makeIdle {
    if (![self canChangeStateTo:PlayerStateIdle]) {
        return;
    }
    
    [self removeActionForKey:kPlayerAnimationKey];
    
    self.texture = [self idleTextureForPlayer];
    self.currentMoveDirection = MoveDirectionNone;
    
    [self changeStateTo:PlayerStateIdle];
}

- (void)evaluateFacingDirection {
    MoveDirection newMoveDirection = MoveDirectionNone;
    
    CGFloat currentX = lroundf(self.position.x);
    CGFloat currentY = lroundf(self.position.y);
    CGFloat lastX = lroundf(self.lastPosition.x);
    CGFloat lastY = lroundf(self.lastPosition.y);
    
    if (currentX > lastX) {
        //NSLog(@"Moving Right x:%f y:%f", currentX, currentY);
        newMoveDirection = MoveDirectionRight;
    } else if (currentX < lastX) {
        //NSLog(@"Moving Left x:%f y:%f", currentX, currentY);
        newMoveDirection = MoveDirectionLeft;
    } else if (currentY > lastY) {
        //NSLog(@"Moving Backward x:%f y:%f", currentX, currentY);
        newMoveDirection = MoveDirectionBackward;
    } else if (currentY < lastY) {
        //NSLog(@"Moving Forward x:%f y:%f", currentX, currentY);
        newMoveDirection = MoveDirectionForward;
    } else {
        return;
    }

    if (newMoveDirection != self.currentMoveDirection) {
        self.currentMoveDirection = newMoveDirection;
        switch (self.currentMoveDirection) {
            case MoveDirectionRight:
                [self runAction:self.rightMoveStrategy withKey:kPlayerAnimationKey];
                break;
            case MoveDirectionForward:
                [self runAction:self.forwardMoveStrategy withKey:kPlayerAnimationKey];
                break;
            case MoveDirectionLeft:
                [self runAction:self.leftMoveStrategy withKey:kPlayerAnimationKey];
                break;
            case MoveDirectionBackward:
                [self runAction:self.backwardMoveStrategy withKey:kPlayerAnimationKey];
                break;
            default:
                break;
        }
    }
    
    self.lastPosition = self.position;
}


#pragma mark ***** Private Methods *****

- (SKTexture *)idleTextureForPlayer {
    return [self textureFromAtlas:CGPointMake(0, 0)];
}

- (SKTexture *)textureFromAtlas:(CGPoint)point {
    CGImageRef cgIcon = CGImageCreateWithImageInRect(self.atlas.CGImage, CGRectMake(point.x, point.y, 48, 48));
    SKTexture *texture = [SKTexture textureWithCGImage:cgIcon];
    CGImageRelease(cgIcon);
    
    return texture;
}

- (SKAction *)forwardMoveStrategy {
    NSArray *textures = @[
        [self textureFromAtlas:CGPointMake(0, 0)],
        [self textureFromAtlas:CGPointMake(0, 48)],
        [self textureFromAtlas:CGPointMake(0, 96)],
        [self textureFromAtlas:CGPointMake(0, 144)]
    ];
    
    return [SKAction repeatActionForever:[SKAction animateWithTextures:textures timePerFrame:0.20]];
}

- (SKAction *)leftMoveStrategy {
    NSArray *textures = @[
        [self textureFromAtlas:CGPointMake(48, 0)],
        [self textureFromAtlas:CGPointMake(48, 48)],
        [self textureFromAtlas:CGPointMake(48, 96)],
        [self textureFromAtlas:CGPointMake(48, 144)]
    ];
    
    return [SKAction repeatActionForever:[SKAction animateWithTextures:textures timePerFrame:0.20]];
}

- (SKAction *)backwardMoveStrategy {
    NSArray *textures = @[
        [self textureFromAtlas:CGPointMake(96, 0)],
        [self textureFromAtlas:CGPointMake(96, 48)],
        [self textureFromAtlas:CGPointMake(96, 96)],
        [self textureFromAtlas:CGPointMake(96, 144)]
    ];
    
    return [SKAction repeatActionForever:[SKAction animateWithTextures:textures timePerFrame:0.20]];
}

- (SKAction *)rightMoveStrategy {
    NSArray *textures = @[
        [self textureFromAtlas:CGPointMake(144, 0)],
        [self textureFromAtlas:CGPointMake(144, 48)],
        [self textureFromAtlas:CGPointMake(144, 96)],
        [self textureFromAtlas:CGPointMake(144, 144)]
    ];
    
    return [SKAction repeatActionForever:[SKAction animateWithTextures:textures timePerFrame:0.20]];
}

- (void)changeStateToPlayerStateStopping {
    [self changeStateTo:PlayerStateStopping];
}


#pragma mark ***** Encoding *****

static NSString * const kStateKey = @"State";
static NSString * const kAtlasKey = @"Atlas";
static NSString * const kLastPositionKey = @"LastPosition";
static NSString * const kStartPositionKey = @"StartPosition";

- (void)encodeWithCoder:(NSCoder *)coder {
    [super encodeWithCoder:coder];
    
    [coder encodeInteger:self->_state forKey:kStateKey];
    [coder encodeObject:UIImagePNGRepresentation(self->_atlas) forKey:kAtlasKey];
    [coder encodeCGPoint:self->_lastPosition forKey:kLastPositionKey];
    [coder encodeCGPoint:self->_startPosition forKey:kStartPositionKey];
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        self->_state = [coder decodeIntegerForKey:kStateKey];
        [self setAtlas:[UIImage imageWithData:[coder decodeObjectForKey:kAtlasKey]]];
        self->_lastPosition = [coder decodeCGPointForKey:kLastPositionKey];
        self->_startPosition = [coder decodeCGPointForKey:kStartPositionKey];
        
        self->_forwardMoveStrategy = [self forwardMoveStrategy];
        self->_leftMoveStrategy = [self leftMoveStrategy];
        self->_backwardMoveStrategy = [self backwardMoveStrategy];
        self->_rightMoveStrategy = [self rightMoveStrategy];
    }
    
    return self;
}

@end
