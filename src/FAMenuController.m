//
//  FAMenuController.m
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 21/08/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FAMenuController.h"

@interface FAMenuController ()

@end

@implementation FAMenuController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerBackgroundNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark ***** NotificationCenter *****

- (void)registerBackgroundNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
}

- (void)unregisterBackgroundNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)applicationWillResignActive:(NSNotification *)notification {
    //[self performSegueWithIdentifier:@"UnwindContinueSegue" sender:self];
}

- (void)dealloc {
    [self unregisterBackgroundNotifications];
}

/*
#pragma mark ***** Navigation *****

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
