//
//  FAPathManager.m
//  TileMapDemo
//
//  Created by Mark Brownsword on 19/03/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FAPathManager.h"
#import "FAPathStep.h"

@interface FAPathManager ()

@property (nonatomic, retain) NSMutableArray *openSteps;
@property (nonatomic, retain) NSMutableArray *closedSteps;

@end

@implementation FAPathManager {
@private

}


#pragma mark ***** Public Methods *****

- (NSMutableArray *)calculatePathSteps {
    NSMutableArray *result = [NSMutableArray array];
    self.openSteps = [NSMutableArray array];
    self.closedSteps = [NSMutableArray array];
    
    [self addOpenStep:[[FAPathStep alloc] initWithPosition:self.startPosition]]; // (96, 928) Tile 1,1
	
	do {
        FAPathStep *currentStep = [self.openSteps objectAtIndex:0];
        [self.closedSteps addObject:currentStep];
        [self.openSteps removeObjectAtIndex:0];
        
        if (CGPointEqualToPoint(currentStep.position, self.endPosition)) {
			do {
                if (currentStep.parent != nil) {
                    [result insertObject:currentStep atIndex:0];
                }
                currentStep = currentStep.parent;
            } while (currentStep != nil);
            
			[self.openSteps removeAllObjects];
			[self.closedSteps removeAllObjects];
            self.startPosition = CGPointZero;
            self.endPosition = CGPointZero;
			continue; // End
		}
        
        NSArray *tiles = [self.delegate walkableAdjacentTilesForPosition:currentStep.position];
        for (SKSpriteNode *tile in tiles) {
            FAPathStep *step = [[FAPathStep alloc] initWithPosition:tile.position];
            
            if ([self.closedSteps containsObject:step]) {
				continue;
			}
            
			int moveCost = [self costToMoveFromStep:currentStep toAdjacentStep:step];
            
            NSUInteger index = [self.openSteps indexOfObject:step];
            if (index == NSNotFound) {
				step.parent = currentStep;
				step.gScore = currentStep.gScore + moveCost;
				step.hScore = [self computeHScoreFromCoord:step.position toCoord:self.endPosition];
				
				[self addOpenStep:step];
            }
			else {
                step = [self.openSteps objectAtIndex:index];
				
				if ((currentStep.gScore + moveCost) < step.gScore) {
					step.gScore = currentStep.gScore + moveCost;
                    
					[self.openSteps removeObjectAtIndex:index];
					[self addOpenStep:step];
				}
            }
        }
        
    } while ([self.openSteps count] > 0);
    
    return result;
}


#pragma mark ***** Private Methods *****

- (void)addOpenStep:(FAPathStep *)newStep {
    int i = 0;
	for (; i < self.openSteps.count; i++) {
        FAPathStep *currentStep = [self.openSteps objectAtIndex:i];
		if ([newStep fScore] <= [currentStep fScore]) {
			break;
		}
	}
    
    [self.openSteps insertObject:newStep atIndex:i];
}

- (int)costToMoveFromStep:(FAPathStep *)fromStep toAdjacentStep:(FAPathStep *)toStep {
	return ((fromStep.position.x != toStep.position.x) && (fromStep.position.y != toStep.position.y)) ? 14 : 10;
}

- (int)computeHScoreFromCoord:(CGPoint)fromCoord toCoord:(CGPoint)toCoord {
	return abs(toCoord.x - fromCoord.x) + abs(toCoord.y - fromCoord.y);
}


#pragma mark ***** Encoding *****

static NSString * const kOpenStepsKey = @"OpenSteps";
static NSString * const kClosedStepsKey = @"ClosedSteps";

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeObject:self.openSteps forKey:kOpenStepsKey];
    [coder encodeObject:self.closedSteps forKey:kClosedStepsKey];
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super init]) {
        self.openSteps = [[coder decodeObjectForKey:kOpenStepsKey] mutableCopy];
        self.closedSteps = [[coder decodeObjectForKey:kClosedStepsKey] mutableCopy];
    }
    
    return self;
}

@end
