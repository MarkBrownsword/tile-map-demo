//
//  FAPathStep.m
//  TileMapDemo
//
//  Created by Mark Brownsword on 19/03/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FAPathStep.h"

@implementation FAPathStep

- (id)initWithPosition:(CGPoint)position {
	if ((self = [super init])) {
		self->_position = position;
		self->_gScore = 0;
		self->_hScore = 0;
		self->_parent = nil;
	}
	return self;
}

- (NSString *)description {
	return [NSString stringWithFormat:@"%@  pos=[%.0f;%.0f]  g=%d  h=%d  f=%d", [super description], self.position.x, self.position.y, self.gScore, self.hScore, [self fScore]];
}

- (BOOL)isEqual:(FAPathStep *)other {
	return CGPointEqualToPoint(self.position, other.position);
}

- (int)fScore {
	return self.gScore + self.hScore;
}


#pragma mark ***** Encoding *****

static NSString * const kPositionKey = @"Position";
static NSString * const kGScoreKey = @"GScore";
static NSString * const kHScoreKey = @"HScore";
static NSString * const kParentKey = @"Parent";

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeCGPoint:self->_position forKey:kPositionKey];
    [coder encodeInt:self->_gScore forKey:kGScoreKey];
    [coder encodeInt:self->_hScore forKey:kHScoreKey];
    [coder encodeObject:self->_parent forKey:kParentKey];
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super init]) {
        self->_position = [coder decodeCGPointForKey:kPositionKey];
        self->_gScore = [coder decodeIntForKey:kGScoreKey];
        self->_hScore = [coder decodeIntForKey:kHScoreKey];
        self->_parent = [coder decodeObjectForKey:kParentKey];
    }
    
    return self;
}

@end
