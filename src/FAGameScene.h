//
//  FAMyScene.h
//  src
//

//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FATileMapDelegate.h"
#import "FAPlayer.h"
#import "FAPathManager.h"
#import "JSTileMap.h"
#import "FAGameDelegate.h"

@interface FAGameScene : SKScene <FATileMapDelegate>

@property (nonatomic, weak) id<FAGameDelegate> gameDelegate;

- (NSInteger)collectiblesCount;
- (void)reset;

@end
