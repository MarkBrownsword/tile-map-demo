//
//  FAHudView.h
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 19/04/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FAHudDelegate.h"
#import "FACollectible.h"

@interface FAHudView : UIView

@property (nonatomic, weak) id<FAHudDelegate> delegate;

- (id)initWithFrame:(CGRect)frame countCollectibles:(NSInteger)count;

- (void)addCollectible:(FACollectible *)collectible;
- (void)reset;
- (void)disableHud;
- (void)enableHud;

@end
