//
//  FACollectible.m
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 21/04/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FACollectible.h"

@implementation FACollectible

- (instancetype)initWithIdentifier:(NSUInteger)identifier character:(NSString *)unicode positionOnMap:(CGPoint)point {
    if ((self = [super init])) {
        self->_identifer = identifier;
        self->_found = NO;
        
        self.text = unicode;
        self.position = point;
        self.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        self.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
        self.fontSize = 16;
    }
    
    return self;
}


#pragma mark ***** Encoding *****

static NSString * const kIdentifierKey = @"Identifier";
static NSString * const kFoundKey = @"Found";

- (void)encodeWithCoder:(NSCoder *)coder {
    [super encodeWithCoder:coder];
    [coder encodeInteger:self->_identifer forKey:kIdentifierKey];
    [coder encodeBool:self->_found forKey:kFoundKey];
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        self->_identifer = [coder decodeIntegerForKey:kIdentifierKey];
        self->_found = [coder decodeBoolForKey:kFoundKey];
    }
    
    return self;
}

@end
