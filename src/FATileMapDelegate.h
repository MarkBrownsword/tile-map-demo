//
//  FATileMapDelegate.h
//  TileMapDemo
//
//  Created by Mark Brownsword on 27/02/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

@protocol FATileMapDelegate <NSObject>

- (NSArray *)walkableAdjacentTilesForPosition:(CGPoint)position;
- (SKSpriteNode *)currentTile;

@end
