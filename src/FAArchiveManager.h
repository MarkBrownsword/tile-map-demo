//
//  FAArchiveManager.h
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 12/08/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FAArchiveManager : NSObject

+ (BOOL)encodeScene:(id)scene forKey:(NSString *)key;
+ (id)decodeSceneForKey:(NSString *)key;

@end
