//
//  FAPathStep.h
//  TileMapDemo
//
//  Created by Mark Brownsword on 19/03/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.

@interface FAPathStep : NSObject

@property (nonatomic, assign) CGPoint position;
@property (nonatomic, assign) int gScore;
@property (nonatomic, assign) int hScore;
@property (nonatomic, assign) FAPathStep *parent;

- (id)initWithPosition:(CGPoint)position;
- (int)fScore;

@end
