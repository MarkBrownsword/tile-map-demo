//
//  FAConstants.h
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 22/04/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

@interface FAConstants : NSObject

extern CGFloat const HUD_BACKGROUND_COLOUR_WITH_WHITE;
extern CGFloat const HUD_BACKGROUND_COLOUR_ALPHA;

@end
