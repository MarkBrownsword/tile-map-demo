//
//  FAMyScene.m
//  src
//
//  Created by Mark Brownsword on 18/02/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FAGameScene.h"
#import "FAPathManager.h"
#import "FAPathStep.h"

@interface FAGameScene ()

@property (nonatomic, strong) JSTileMap *map;
@property (nonatomic, strong) TMXLayer *background;
@property (nonatomic, strong) TMXObjectGroup *meta;
@property (nonatomic, strong) FAPlayer *player;
@property (nonatomic, strong) FAPathManager *pathManager;
@property (nonatomic, strong) NSMutableArray *collectibles;
@property (nonatomic, strong) SKNode *worldNode;
@property (nonatomic, assign) CGPoint endPosition;
@property (nonatomic, assign) CGPoint startPosition;

@end

@implementation FAGameScene {
@private
    BOOL _isArchived;

}

- (instancetype)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        self->_isArchived = NO;
    }
    
    return self;
}

- (void)didMoveToView:(SKView *)view {
    self.worldNode = [SKNode node];
    
    if (!self->_isArchived) {
        self.startPosition = [self startPositionInBackground:self.background withMeta:self.meta];
        self.endPosition = [self endPositionInBackground:self.background withMeta:self.meta];
        self.player = [[FAPlayer alloc] initWithAtlas:[self playerImage] atPosition:self.startPosition];
        self.pathManager = [[FAPathManager alloc] init];
        self.collectibles = [self createCollectiblesInBackground:self.background withMeta:self.meta];
    }
    
    self.player.delegate = self;
    self.pathManager.delegate = self;
    
    [self.worldNode addChild:self.map];
    
    for (FACollectible *collectible in self.collectibles) {
        [self.worldNode addChild:collectible];
        if (collectible.found) { // When scene is restored, collectibles might already be found
            [self.gameDelegate playerFoundCollectible:collectible];
        }
    }
    
    [self.worldNode addChild:[self createExitAtPosition:self.endPosition]];
    [self.worldNode addChild:self.player];
    
    [self addChild:self.worldNode];
    
    // Show ChatBox if player is in the start tile
    if (CGPointEqualToPoint(self.startPosition, self.player.position)) {
        [self.gameDelegate showContinue];
    }
}

- (JSTileMap *)map{
    if (self->_map == nil) {
        self->_map = [JSTileMap mapNamed:@"sample.tmx"];
    }
    
    return self->_map;
}

- (TMXLayer *)background {
    if (self->_background == nil) {
        self->_background = [self.map layerNamed:@"background"];
    }
    
    return self->_background;
}

- (TMXObjectGroup *)meta {
    if (self->_meta == nil) {
        self->_meta = [self.map groupNamed:@"meta"];
    }
    
    return self->_meta;
}


#pragma mark ***** Run Loop Events *****

- (void)update:(CFTimeInterval)currentTime {
    if (![self.gameDelegate canUpdate]) {
        return;
    }
    
    switch (self.player.state) {
        case PlayerStatePendingMove:
            [self evaluatePendingMove];
            break;
        case PlayerStateMoving:
            [self.player evaluateFacingDirection];
            break;
        case PlayerStateForceStop:
            [self.player forceStop];
            break;
        case PlayerStateStopping:
            [self.player makeIdle];
            break;
        case PlayerStateIdle:
            [self resume];
            break;
        default:
            break;
    }
}

- (void)didEvaluateActions {
    if (![self.gameDelegate canUpdate] || ![self.player isIdle]) {
        return;
    }
    
    // Check Tile
    SKSpriteNode *tile = [self.background tileAt:self.player.position];
//    NOTE: Might need to use this instead of Player-isIdle if want to e.g. pick up collectibles without stopping
//    if (![self checkPlayerAtTileCentre:tile]) {
//        return;
//    }
    
    // Check Collectibles
    if ([self collectiblesFound]) {
        if (CGPointEqualToPoint(self.endPosition, tile.position)) {
            [self.gameDelegate sceneComplete]; // You Win!
        }
    } else {
        FACollectible *collectible = [self collectibleAtPoint:tile.position];
        if (collectible) {
            [self.gameDelegate playerFoundCollectible:collectible];
        }
    }
}


#pragma mark ***** Touch Events *****

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (![self.player canChangeStateTo:PlayerStatePendingMove]) {
        return;
    }
    
    CGPoint touchPoint = [touches.anyObject locationInNode:self];
    if ([self.player containsPoint:touchPoint]) {
        return; // User tapped player
    }
    
    SKSpriteNode *destinationMapTile = [self tileAtLocation:touchPoint];
    if (![self isWalkableTileLocation:destinationMapTile.position]) {
        return; // User tapped Wall
    }
    
    self.pathManager.endPosition = destinationMapTile.position;
    [self.player changeStateTo:PlayerStatePendingMove];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}


#pragma mark ***** FATileMapDelegate Methods *****

- (NSArray *)walkableAdjacentTilesForPosition:(CGPoint)position {
	NSMutableArray *result = [NSMutableArray arrayWithCapacity:4];
    CGPoint tileCoordAtPosition = [self.background coordForPoint:position];
    
    // Top
    SKSpriteNode *node = [self walkableTileAtCoord:CGPointMake(tileCoordAtPosition.x, tileCoordAtPosition.y - 1)];
    if (node) {
       [result addObject:node];
    }

    // Left
    node = [self walkableTileAtCoord:CGPointMake(tileCoordAtPosition.x - 1, tileCoordAtPosition.y)];
    if (node) {
        [result addObject:node];
    }
	
    // Bottom
    node = [self walkableTileAtCoord:CGPointMake(tileCoordAtPosition.x, tileCoordAtPosition.y + 1)];
    if (node) {
        [result addObject:node];
    }
	
	// Right
    node = [self walkableTileAtCoord:CGPointMake(tileCoordAtPosition.x + 1, tileCoordAtPosition.y)];
    if (node) {
        [result addObject:node];
    }
    
	return [NSArray arrayWithArray:result];
}

- (SKSpriteNode *)currentTile {
    return [self.background tileAt:self.player.position];
}


#pragma mark ***** Public Methods *****

- (NSInteger)collectiblesCount {
    return [[self.meta objectsNamed:@"collectible"] count];
}

- (void)reset {
    // Initialise the collectible items
    for (FACollectible *item in self.collectibles) {
        if (item.parent != nil) {
            [item removeFromParent];
        }
        
        item.found = NO;
        [self.worldNode addChild:item];
    }
    
    // Initialise a new Player
    [self.player removeFromParent];
    self.player = [[FAPlayer alloc] initWithAtlas:[self playerImage] atPosition:self.startPosition];
    
    // Add Player and show ChatBox
    [self.worldNode addChild:self.player];
    [self.gameDelegate showContinue];
}


#pragma mark ***** Private Methods *****

- (BOOL)isWalkableTileLocation:(CGPoint)location { // By Screen Point
    CGPoint coord = [self.background coordForPoint:location];
    return [self isWalkableTileCoord:coord];
}

- (BOOL)isWalkableTileCoord:(CGPoint)coord { // By Map Coordinate
    int tileGid = [self.background.layerInfo tileGidAtCoord:coord];
    
    NSDictionary *properties = [self.map propertiesForGid:tileGid];
    if (!properties) {
        return NO;
    }
    
    NSString *property = properties[@"isWalkable"];
    return property && [property isEqualToString:@"True"];
}

- (SKSpriteNode *)walkableTileAtCoord:(CGPoint)coord {
    SKSpriteNode *tileAtCoord = [self.background tileAtCoord:coord];
    if (!tileAtCoord) {
        return nil;
    }
    
    if (![self isWalkableTileCoord:coord]) {
        return nil;
    }
    
	return tileAtCoord;
}

- (SKSpriteNode *)tileAtLocation:(CGPoint)location { // By Screen Point
    return [self.background tileAt:location];
}

- (FACollectible *)collectibleAtPoint:(CGPoint)point {
    for (FACollectible *item in self.collectibles) {
        if (CGPointEqualToPoint(item.position, point) && item.found == NO) {
            return item;
        }
    }
    
    return nil;
}

- (BOOL)collectiblesFound {
    for (FACollectible *item in self.collectibles) {
        if (!item.found) {
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)checkPlayerAtTileCentre:(SKSpriteNode *)tile {
    return CGPointEqualToPoint(self.player.position, tile.position);
}

- (void)evaluatePendingMove {
    SKSpriteNode *tile = [self.background tileAt:self.player.position];
    if (![self checkPlayerAtTileCentre:tile]) {
        return; // Not at tile centre
    }
    
    if (CGPointEqualToPoint(tile.position, self.pathManager.endPosition)) {
        [self.player changeStateTo:PlayerStateStopping];
        return; // Start, End are same
    }
    
    self.pathManager.startPosition = tile.position; // End position is set in TouchesBegan
    [self.player moveAlongPath:[self.pathManager calculatePathSteps]];
}

- (CGPoint)startPositionInBackground:(TMXLayer *)background withMeta:(TMXObjectGroup *)meta {
    NSDictionary *startObject = [meta objectNamed:@"start"];
    SKSpriteNode *startTile = [background tileAt:CGPointMake([startObject[@"x"] floatValue],
                                                             [startObject[@"y"] floatValue])];
    return startTile.position; // meta object may not be at tile centre, so use startTile.position 
}

- (CGPoint)endPositionInBackground:(TMXLayer *)background withMeta:(TMXObjectGroup *)meta {
    NSDictionary *endObject = [meta objectNamed:@"end"];
    SKSpriteNode *endTile = [background tileAt:CGPointMake([endObject[@"x"] floatValue],
                                                           [endObject[@"y"] floatValue])];
    return endTile.position; // meta object may not be at tile centre, so use startTile.position
}

- (UIImage *)playerImage {
    return [UIImage imageNamed:@"ada_0_0"];
}

- (NSMutableArray *)createCollectiblesInBackground:(TMXLayer *)background withMeta:(TMXObjectGroup *)meta {
    NSMutableArray *result = [[NSMutableArray alloc] init];
    NSArray *collectibles = [meta objectsNamed:@"collectible"];
    
    for (int i = 0; i < collectibles.count; i++) {
        NSDictionary *collectible = collectibles[i];
        NSString *unicode = @"\U0001f31f"; // Unicode character
        SKSpriteNode *tile = [background tileAt:CGPointMake([collectible[@"x"] floatValue], [collectible[@"y"] floatValue])];
        [result addObject:[[FACollectible alloc] initWithIdentifier:i character:unicode  positionOnMap:tile.position]];
    }
    
    return result;
}

- (SKLabelNode *)createExitAtPosition:(CGPoint)position {
    SKLabelNode *exit = [[SKLabelNode alloc] init];
    exit.text = @"\U0001f3af"; // Unicode character
    exit.position = position;
    exit.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    exit.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    exit.fontSize = 16;
    
    return exit;
}

- (void)resume {
    self.paused = NO;
    
    if (self.player.isIdle) {
        self.pathManager.endPosition = self.endPosition;
        [self.player changeStateTo:PlayerStatePendingMove];
    }
}


#pragma mark ***** Encoding *****

static NSString * const kStartPositionKey = @"StartPosition";
static NSString * const kEndPositionKey = @"EndPosition";
static NSString * const kPlayerKey = @"Player";
static NSString * const kPathManagerKey = @"PathManager";
static NSString * const kCollectiblesKey = @"Collectibles";

- (void)encodeWithCoder:(NSCoder *)coder {
    [coder encodeCGPoint:self->_startPosition forKey:kStartPositionKey];
    [coder encodeCGPoint:self->_endPosition forKey:kEndPositionKey];
    [coder encodeObject:[self playerForEncoding] forKey:kPlayerKey];
    [coder encodeObject:self->_pathManager forKey:kPathManagerKey];
    [coder encodeObject:self->_collectibles forKey:kCollectiblesKey];
    
    [self->_worldNode removeFromParent]; // JSTileMap has encoding issues, so skip encoding. We can construct worldNode manually in didMoveToView:
    [super encodeWithCoder:coder];
    
    [self addChild:self->_worldNode]; // Add back, because the game may still be in memory when user returns
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        self->_startPosition = [coder decodeCGPointForKey:kStartPositionKey];
        self->_endPosition = [coder decodeCGPointForKey:kEndPositionKey];
        self->_player = [coder decodeObjectForKey:kPlayerKey];
        self->_pathManager = [coder decodeObjectForKey:kPathManagerKey];
        self->_collectibles = [[coder decodeObjectForKey:kCollectiblesKey] mutableCopy];
        
        self->_isArchived = YES;
    }
    
    return self;
}

- (FAPlayer *)playerForEncoding {
    if (self->_player.isIdle) {
        return self->_player;
    }
    
    // When player is moving, archive a new player at current tile position.
    // This avoids all the issues around not being able to archive running actions.
    SKSpriteNode *tile = [self.background tileAt:self.player.position];
    return [[FAPlayer alloc] initWithAtlas:[self playerImage] atPosition:tile.position withStartPosition:[self startPositionInBackground:self->_background withMeta:self->_meta]];
}

@end
