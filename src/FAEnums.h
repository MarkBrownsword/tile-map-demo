//
//  FAEnums.h
//  TileMapDemo
//
//  Created by Mark Brownsword on 25/03/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

@interface FAEnums : NSObject

typedef NS_ENUM(NSInteger, PlayerState) {
    PlayerStateIdle,
    PlayerStatePendingMove,
    PlayerStateMoving,
    PlayerStateStopping,
    PlayerStateForceStop
};

typedef NS_ENUM(NSInteger, MoveDirection) {
    MoveDirectionNone,
    MoveDirectionForward,
    MoveDirectionLeft,
    MoveDirectionBackward,
    MoveDirectionRight
};

@end
