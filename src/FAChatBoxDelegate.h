//
//  FAChatBoxDelegate.h
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 27/08/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

@protocol FAChatBoxDelegate <NSObject>

- (void)resumeGame;

@end
