//
//  FAGameDelegate.h
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 12/04/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FACollectible.h"

@protocol FAGameDelegate <NSObject>

- (BOOL)canUpdate;
- (void)showQuest;
- (void)showContinue;
- (void)sceneComplete;
- (void)playerFoundCollectible:(FACollectible *)collectible;

@end
