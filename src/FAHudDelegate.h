//
//  FAHudDelegate.h
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 26/08/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

@protocol FAHudDelegate <NSObject>

- (void)showMenu;

@end
