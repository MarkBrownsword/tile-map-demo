//
//  main.m
//  src
//
//  Created by Mark Brownsword on 18/02/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FAAppDelegate class]));
    }
}
