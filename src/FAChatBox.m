//
//  FAChatBox.m
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 9/04/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FAChatBox.h"

@interface FAChatBox ()

@end

@implementation FAChatBox {
    NSDictionary *headingAttributes;
    NSDictionary *bodyAttributes;
    CGRect headingRect;
    CGRect textRect;
    CGRect backgroundRect;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createTextAttributes];
        [self composeView];
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    // draw Background
    [[UIColor colorWithWhite:HUD_BACKGROUND_COLOUR_WITH_WHITE alpha:HUD_BACKGROUND_COLOUR_ALPHA] setFill];
    [[UIBezierPath bezierPathWithRoundedRect:backgroundRect cornerRadius:10.0f] fill];
    
    // draw Text
    [self.headingText drawInRect:headingRect withAttributes:headingAttributes];
    [self.bodyText drawInRect:textRect withAttributes:bodyAttributes];
    
    // animate
    self.alpha = 0.0f;
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 1.0f;
    }];
}


#pragma mark ***** Touch Events *****

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.delegate resumeGame];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}


#pragma mark ***** Private Methods *****

- (void)createTextAttributes {
    CGFloat positionX = 5;
    CGFloat positionHeadingY = 5;
    CGFloat positionBodyY = 40;
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    
    self->headingRect = CGRectMake(positionX, positionHeadingY, width, positionBodyY);
    self->textRect = CGRectMake(positionX, positionBodyY, width, height - positionBodyY);
    
    self->headingAttributes = @{
        NSFontAttributeName: [UIFont systemFontOfSize:26.0f],
        NSUnderlineStyleAttributeName: @1, // NSUnderlineStyleSingle
        NSForegroundColorAttributeName: [UIColor whiteColor]
    };
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
    
    self->bodyAttributes = @{
        NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0f],
        NSParagraphStyleAttributeName: paragraphStyle,
        NSForegroundColorAttributeName: [UIColor whiteColor]
    };
}

- (void)composeView {
    self->backgroundRect = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    
    self.backgroundColor = [UIColor clearColor];
    self.hidden = YES;
}

@end
