//
//  FAConstants.m
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 22/04/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FAConstants.h"

@implementation FAConstants

CGFloat const HUD_BACKGROUND_COLOUR_WITH_WHITE = 0.3f;
CGFloat const HUD_BACKGROUND_COLOUR_ALPHA = 0.5f;

@end
