//
//  FACollectible.h
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 21/04/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

@interface FACollectible : SKLabelNode

@property (nonatomic, assign) NSUInteger identifer;
@property (nonatomic, assign) BOOL found;

- (instancetype)initWithIdentifier:(NSUInteger)identifier character:(NSString *)unicode positionOnMap:(CGPoint)point;

@end
