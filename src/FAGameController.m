//
//  FAViewController.m
//  src
//
//  Created by Mark Brownsword on 18/02/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FAGameController.h"
#import "FAGameScene.h"
#import "FAPlayer.h"
#import "JSTileMap.h"
#import "FAChatBox.h"
#import "FAGameDelegate.h"
#import "FAHudView.h"
#import "FAArchiveManager.h"
#import "FAMenuController.h"
#import "FAHudDelegate.h"
#import "FAChatBoxDelegate.h"

@interface FAGameController () <FAGameDelegate, FAHudDelegate, FAChatBoxDelegate>

@property (nonatomic, strong) SKView *gameView;
@property (nonatomic, strong) FAGameScene *gameScene;
@property (nonatomic, strong) FAChatBox *chatBox;
@property (nonatomic, strong) FAHudView *hudView;
@property (nonatomic, strong) UIButton *continueButton;

@end

@implementation FAGameController {
    BOOL _gameOver;
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];

    if (self.gameView) {
        return;
    }
    
    self->_gameOver = NO;
    
    // Initialise GameScene
    self.gameScene.gameDelegate = self;
    
    // Initialise GameView
    self.gameView = [[SKView alloc] initWithFrame:self.view.bounds];
#ifdef DEBUG
    self.gameView.showsFPS = YES;
    self.gameView.showsNodeCount = YES;
#endif
    
    // Initialise ChatBox
    self.chatBox = [[FAChatBox alloc] initWithFrame:[self createChatBoxRect]];
    self.chatBox.delegate = self;
    
    // Initialise HudView
    self.hudView = [[FAHudView alloc] initWithFrame:[self createHudViewRect] countCollectibles:[self.gameScene collectiblesCount]];
    self.hudView.delegate = self;
    
    // Initialise View
    [self.view addSubview:self.gameView];
    [self.view insertSubview:self.chatBox aboveSubview:self.gameView];
    [self.view insertSubview:self.hudView aboveSubview:self.gameView];
    [self.view insertSubview:self.continueButton aboveSubview:self.gameView];
    
    // Present Scene
    [self.gameView presentScene:self.gameScene];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}


#pragma mark ***** Properties *****

- (FAGameScene *)gameScene {
    if (self->_gameScene == nil) {
        self->_gameScene = [[FAGameScene alloc] initWithSize:self.view.bounds.size];
        self->_gameScene.scaleMode = SKSceneScaleModeAspectFill;
    }
    
    return self->_gameScene;
}

- (UIButton *)continueButton {
    if (self->_continueButton == nil) {
        self->_continueButton = [[UIButton alloc] initWithFrame:({
            CGFloat width = 400;
            CGFloat height = 90;
            CGFloat frameX =  self.gameScene.size.width / 2 - width / 2;
            CGFloat frameY = self.gameScene.size.height / 2 - height / 2;
            
            CGRectMake(frameX, frameY, width, height);
        })];
        
        self->_continueButton.hidden = YES;
        self->_continueButton.clipsToBounds = YES;
        self->_continueButton.layer.cornerRadius = 10;
        self->_continueButton.backgroundColor = [UIColor colorWithWhite:HUD_BACKGROUND_COLOUR_WITH_WHITE alpha:HUD_BACKGROUND_COLOUR_ALPHA];
        self->_continueButton.titleLabel.font = [UIFont fontWithName:@"Noteworthy" size:60.0f];
        self->_continueButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 10, 0);
        
        [self->_continueButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    }
    
    return self->_continueButton;
}


#pragma mark ***** FAHudDelegate Methods

- (void)showMenu {
    [self performSegueWithIdentifier:@"MenuSegue" sender:self];
}


#pragma mark ***** FAChatBoxDelegate *****

- (void)resumeGame {
    if (self->_gameOver) {
        [self.hudView reset];
        [self.gameScene reset];
        
        self->_gameOver = NO;
    }
    
    self.continueButton.hidden = YES;
    [self.hudView enableHud];
    
    self.gameScene.paused = NO;
}


#pragma mark ***** FAGameDelegate Methods *****

- (BOOL)canUpdate {
    return self.continueButton.hidden && self.chatBox.hidden && self->_gameOver == NO;
}

- (void)showQuest {
    if (self.chatBox.hidden == NO) {
        return;
    }
    
    self.gameScene.paused = YES;
    
    [self.hudView disableHud];
    [self showChatBoxWithHeading:@"YOUR QUEST" andBody:@"Your quest is to collect the stars and proceed to the exit. Tap here to start."];
}

- (void)showContinue {
    self.continueButton.hidden = NO;
    [self.hudView disableHud];
    [self.continueButton setTitle:@"Tap to start..." forState:UIControlStateNormal];
    [self.continueButton addTarget:self action:@selector(executeCommand:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)sceneComplete {
    self->_gameOver = YES;
    
    [self.continueButton setTitle:@"You win!" forState:UIControlStateNormal];
    [self.continueButton removeTarget:self action:@selector(executeCommand:) forControlEvents:UIControlEventTouchUpInside];

    self.continueButton.hidden = NO;
}

- (void)playerFoundCollectible:(FACollectible *)collectible {
    collectible.found = YES;
    [collectible removeFromParent];
    [self.hudView addCollectible:collectible];
}


#pragma mark ***** Seque Methods *****

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"MenuSegue"]) {
        self.gameScene.paused = YES;
        [self hideChatBox];
    }
}

- (IBAction)menuViewContinue:(UIStoryboardSegue *)segue {
    self.gameScene.paused = NO;
}

- (IBAction)menuViewRestart:(UIStoryboardSegue *)segue {
    [self reset];
}


#pragma mark ***** Private Methods *****

- (CGRect)createChatBoxRect {
    CGFloat chatBoxHeight = 128;
    CGFloat chatBoxWidth = self.gameView.frame.size.width - 64;
    CGFloat chatBoxY = roundf(self.gameView.frame.size.height - chatBoxHeight - 32);
    CGFloat chatBoxX = 32;
    
    return CGRectMake(chatBoxX, chatBoxY, chatBoxWidth, chatBoxHeight);
}

- (CGRect)createHudViewRect {
    CGFloat hudViewHeight = 42;
    CGFloat hudViewWidth = self.gameView.frame.size.width;
    CGFloat hudViewY = 0;
    CGFloat hudViewX = 0;
    
    return CGRectMake(hudViewX, hudViewY, hudViewWidth, hudViewHeight);
}

- (void)hideChatBox {
    if (self.chatBox.hidden) {
        return;
    }
    
    self.chatBox.hidden = YES;
}

- (void)showChatBoxWithHeading:(NSString *)heading andBody:(NSString *)body {
    self.chatBox.hidden = NO;
    self.chatBox.headingText = heading;
    self.chatBox.bodyText = body;
    
    [self.chatBox setNeedsDisplay]; // Force ChatBox DrawRect
}

- (void)executeCommand:(UIButton *)sender {
    if (sender == self.continueButton) {
        [self resumeGame];
    }
}

- (void)reset {
    [self.hudView reset];
    [self.gameScene reset];
}


#pragma mark ***** State Restoration *****

static NSString * const kShouldDecodeSceneKey = @"ShouldDecodeScene";
static NSString * const kSceneKey = @"Scene";

- (void)encodeRestorableStateWithCoder:(NSCoder *)coder {
    [super encodeRestorableStateWithCoder:coder];
    
    BOOL shouldDecodeScene = [FAArchiveManager encodeScene:self.gameScene forKey:kSceneKey];
    [coder encodeBool:shouldDecodeScene forKey:kShouldDecodeSceneKey];
}

- (void)decodeRestorableStateWithCoder:(NSCoder *)coder {
    [super decodeRestorableStateWithCoder:coder];

    if ([coder containsValueForKey:kShouldDecodeSceneKey] &&
        [coder decodeBoolForKey:kShouldDecodeSceneKey]) {
        self.gameScene = [FAArchiveManager decodeSceneForKey:kSceneKey];
    }
}

@end
