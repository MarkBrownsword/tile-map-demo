//
//  FAHudView.m
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 19/04/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FAHudView.h"

@interface FAHudView ()

@property (nonatomic, strong) UIButton *menuButton;
@property (nonatomic, strong) NSMutableArray *collectibleLabels;

@end

@implementation FAHudView {

}

- (id)initWithFrame:(CGRect)frame countCollectibles:(NSInteger)count {
    self = [super initWithFrame:frame];
    if (self) {
        [self createMenuButton];
        [self createCollectibleLabels:count];
        
        [self composeView];
    }
    
    return self;
}


#pragma mark ***** Public Methods *****

- (void)addCollectible:(FACollectible *)collectible {
    UILabel *collectibleLabel = self.collectibleLabels[collectible.identifer];
    collectibleLabel.text = collectible.text;
}

- (void)reset {
    for (UILabel *label in self.collectibleLabels) {
        label.text = @"."; // Placeholder
    }
}

- (void)disableHud {
    [self toggleMenuButton:NO];
}

- (void)enableHud {
    [self toggleMenuButton:YES];
}


#pragma mark ***** Private Methods *****

- (void)composeView {
    [self addSubview:self.menuButton];
    
    for (UILabel *label in self.collectibleLabels) {
        label.text = @".";
        [self addSubview:label];
    }
}

- (void)createMenuButton {
    self.menuButton = [[UIButton alloc] initWithFrame:({
        CGFloat widthHeight = self.frame.size.height; // Width & Height
        CGFloat frameX = 5;
        CGFloat frameY = 5;
        
        CGRect frame = CGRectMake(frameX, frameY, widthHeight, widthHeight);
        frame;
    })];
    
    self.menuButton.backgroundColor = [UIColor clearColor];
    self.menuButton.titleLabel.font = [UIFont boldSystemFontOfSize:32];
    
    [self.menuButton setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
    [self.menuButton setTitle:@"\u2630" forState:UIControlStateNormal]; // Unicode Character
    [self.menuButton addTarget:self action:@selector(executeCommand:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)createCollectibleLabels:(NSInteger)count {
    self.collectibleLabels = [[NSMutableArray alloc] init];
    CGFloat gap = 30;
    CGFloat frameXModifier = 0; // frameXModifier = (self.frame.size.width / 2) - (gap * count / 2);
    
    for (int i = 0; i < count; i++) {
        UILabel *label = [self makeCollectibleLabel:frameXModifier];
        [self.collectibleLabels addObject:label];
        frameXModifier = frameXModifier + gap;
    }
}

- (UILabel *)makeCollectibleLabel:(CGFloat)frameXModifier {
    UILabel *collectibleLabel = ({
        UILabel *label = [[UILabel alloc] initWithFrame:({
            CGFloat widthHeight = self.frame.size.height; // Width & Height
            CGFloat frameX = (self.frame.size.width - widthHeight) - frameXModifier;
            CGFloat frameY = 0;
            
            CGRect frame = CGRectMake(frameX, frameY, widthHeight, widthHeight);
            frame;
        })];
        
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont boldSystemFontOfSize:18];
        label;
    });
    
    return collectibleLabel;
}

- (void)executeCommand:(UIButton *)sender {
    if (sender == self.menuButton) {
        [self.delegate showMenu];
    }
}

- (void)toggleMenuButton:(BOOL)on {
    self.menuButton.enabled = on;
    self.menuButton.hidden = !on;
}

@end
