//
//  FAChatBox.h
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 9/04/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FAChatBoxDelegate.h"

@interface FAChatBox : UIView

@property (nonatomic, weak) id<FAChatBoxDelegate> delegate;
@property (nonatomic, strong) NSString *headingText;
@property (nonatomic, strong) NSString *bodyText;

@end
