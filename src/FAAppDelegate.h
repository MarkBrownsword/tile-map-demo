//
//  FAAppDelegate.h
//  src
//
//  Created by Mark Brownsword on 18/02/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
