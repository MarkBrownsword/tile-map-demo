//
//  FAArchiveManager.m
//  TileMapDemo
//
//  Created by MARK BROWNSWORD on 12/08/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FAArchiveManager.h"

@implementation FAArchiveManager

// + (BOOL)encodeObject:(id)object inDirectory:(NSString *)directory forKey:(NSString *)key
+ (BOOL)encodeScene:(id)scene forKey:(NSString *)key {
    NSString *filePath = [FAArchiveManager sceneArchiveDirectory];
    if (filePath == nil) {
        return false;
    }
    
    return [NSKeyedArchiver archiveRootObject:scene toFile:filePath];
}

// + (id)decodeObjectInDirectory:(NSString *)directory forKey:(NSString *)key
+ (id)decodeSceneForKey:(NSString *)key {
    NSString *filePath = [FAArchiveManager sceneArchiveDirectory];
    if (filePath == nil) {
        return false;
    }
    
    id result = [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
    return result;
}

// + (NSString *)archivePathForDirectory:(NSString *)directory
+ (NSString *)sceneArchiveDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:@"Private Documents"];
    
    NSError *error;
    [[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:&error];
    if (error != nil) {
        return nil;
    }
    
    return [documentsDirectory stringByAppendingPathComponent:@"autosaved-scene"];
}

@end
