//
//  FAPathManager.h
//  TileMapDemo
//
//  Created by Mark Brownsword on 19/03/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FATileMapDelegate.h"

@interface FAPathManager : NSObject

@property (nonatomic, weak) id<FATileMapDelegate> delegate;
@property (assign, nonatomic) CGPoint startPosition;
@property (assign, nonatomic) CGPoint endPosition;

- (NSMutableArray *)calculatePathSteps;

@end
