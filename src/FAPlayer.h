//
//  FAPlayer.h
//  TileMapDemo
//
//  Created by Mark Brownsword on 7/03/2014.
//  Copyright (c) 2014 Faraway Software Studio. All rights reserved.
//

#import "FATileMapDelegate.h"
#import "FAEnums.h"
#import "FACollectible.h"

@interface FAPlayer : SKSpriteNode

@property (nonatomic, weak) id<FATileMapDelegate> delegate;
@property (nonatomic, assign, readonly) PlayerState state;
@property (nonatomic, assign) BOOL isIdle;
@property (nonatomic, assign) BOOL hasPendingMove;

- (instancetype)initWithAtlas:(UIImage *)atlas atPosition:(CGPoint)position;
- (instancetype)initWithAtlas:(UIImage *)atlas atPosition:(CGPoint)position withStartPosition:(CGPoint)startPosition;

- (void)moveAlongPath:(NSMutableArray *)pathSteps;
- (void)forceStop;
- (void)changeStateTo:(PlayerState)state;
- (BOOL)canChangeStateTo:(PlayerState)state;
- (void)makeIdle;
- (void)evaluateFacingDirection;

@end
